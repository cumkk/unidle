var express = require('express');
var router = express.Router();
var pool = require('./db');
var md5 = require('md5');
var multer=require('multer');
var fs=require('fs');

var createFolder = function(folder){
 try{
  fs.accessSync(folder); 
 }catch(e){
  fs.mkdirSync(folder);
 } 
};
 
var uploadFolder = './upload/';
createFolder(uploadFolder);

// 通过 filename 属性定制
var storage = multer.diskStorage({
 destination: function (req, file, cb) {
  cb(null, uploadFolder); // 保存的路径，备注：需要自己创建
 },
 filename: function (req, file, cb) {
  // 将保存文件名设置为 字段名 + 时间戳，比如 logo-1478521468943
  let suffix=file.mimetype.split('/')[1];//获取文件格式
  cb(null, file.fieldname + '-' + Date.now()+'.'+suffix); 
 }
});
 
// 通过 storage 选项来对 上传行为 进行定制化;
var upload = multer({ storage: storage });

router.post('/api/v1/img',upload.single('file'),(req, res, next ) => {
    // console.log(req.file,'------',req.body,'-------',req.file.path);
    res.json({'url':req.file.path,'code':1}) ;
})


/*
     GETPOSTS ROUTES SECTION
*/
router.get('/api/v1/getposts', (req, res, next ) => {
    let id = req.query.post_id;
    pool.query(`SELECT * FROM unidle.posts
          ORDER BY pid DESC Limit 10`,
    (q_err, q_res) => {
        if(q_err) return next('error');
        res.json(q_res.rows)
    })
})



// testing
router.get('/api/hello', (req, res) => {
    res.json('hello world')
})

/*
    POSTS ROUTES SECTION
*/
router.post('/api/post/posttodb', (req, res) => {
    let values = [req.body.title, req.body.body, req.body.user_id, req.body.author,req.body.imgs, req.body.price, req.body.type]
    pool.query('INSERT INTO unidle.posts(title, body, user_id, author, created_date, imgs, price,type)' +
    ' VALUES($1, $2, $3, $4, NOW(), $5, $6, $7)', values, (q_err, q_res) => {
        if(q_err) {
            console.log(q_err)
            res.json({msg:"err", 'code':0})
            
            return;
        }
        console.log(q_res)
        res.json({data:q_res,'code':1})
    })
})

router.delete('/api/delete/post', (req, res, next) => {
  const post_id = req.body.post_id
  pool.query(`DELETE FROM posts WHERE pid = $1`, [ post_id ],
              (q_err, q_res) => {
                res.json(q_res.rows)
                console.log(q_err)
       })
})


router.get('/api/search/title', (req, res, next) => {
    var title = req.query.title;
    pool.query('SELECT * FROM unidle.posts WHERE title LIKE $1', ['%'+title+'%'], (err, result) => {
        if (err) {
            return console.error('Error executing query', err.stack)
        }
        console.log('%'+title+'%')
        res.json(result.rows)
    })
})

/*
  USER PROFILE SECTION
*/
// POST SIGNIN INFO
router.post('/api/v1/signIn', (req, res) => {
    let values = [req.body.username, req.body.email, md5(req.body.password)]
    pool.query('INSERT INTO unidle.users(username, email, password)' +
        ' VALUES($1, $2, $3)', values, (q_err, q_res) => {
        if(q_err) {
            console.log(q_err)
            res.json({msg:"err", 'code':0})

            return;
        }
        console.log(q_res)
        res.json({data:q_res,'code':1})
    })
})

router.get('/api/login', (req, res, next) => {
    let login = [req.query.email]
    pool.query('SELECT password FROM unidle.users WHERE email=$1', login, (q_err, q_res) => {
        if(q_err) {
            console.log(q_err)
            res.json({msg:"err", 'code':0})
            return;
        }
        // console.log(q_res.rows[0].password)
        // console.log(md5(req.query.password))
        if (q_res.rows[0].password == (md5(req.query.password))) {
            // console.log("1")
            res.json(1);
          
        } else {
            // console.log("2")
            res.json(0);
      
        }
    })
})

router.get('/api/get/userprofilefromdb', (req, res, next) => {
    const email = req.query.email
    console.log(email)
    pool.query(`SELECT * FROM users
              WHERE email=$1`, [ email ],
        (q_err, q_res) => {
            res.json(q_res.rows)
        })
} )


router.get('/api/get/userposts', (req, res) => {
    const user_id = req.query.user_id
    console.log(user_id)
    pool.query(`SELECT * FROM posts
              WHERE user_id=$1`, [ user_id ],
        (q_err, q_res) => {
            res.json(q_res.rows)
        })
} )

router.get('/api/get/goodposts', (req, res) => {
    console.log(req.query)
    let pid = req.query.pid
    console.log(pid)
    pool.query(`SELECT * FROM unidle.posts
              WHERE pid=$1`, [ pid ],
        (q_err, q_res) => {
            if(q_err) {
                console.log(q_err)
                res.json({msg:"err", 'code':0})
                return;
            }
            res.json(q_res.rows)
        })
} )

// Retrieve another users profile from db based on user id
router.get('/api/get/otheruserprofilefromdb', (req, res, next) => {
    // const email = [ "%" + req.query.email + "%"]
    const uid = String(req.query.user_id)
    pool.query(`SELECT * FROM users
              WHERE uid = $1`,
        [ uid ], (q_err, q_res) => {
            res.json(q_res.rows)
        });
});


module.exports = router;
