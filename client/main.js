import Vue from 'vue'
import App from './App'
import store from './store'
import cuCustom from './colorui/components/cu-custom.vue'
Vue.component('cu-custom',cuCustom)
Vue.config.productionTip = false
Vue.prototype.$store = store
Vue.prototype.$gopage = function (url) {
    if (url.indexOf('pages/index/index')>-1||url.indexOf('pages/post/index')>-1||url.indexOf('pages/buy/cart')>-1||url.indexOf('pages/my/user')>-1) {
        uni.switchTab({url:url});return;
    }
    uni.navigateTo({url:url});
}
Vue.prototype.$godetail = function(id) {
		uni.navigateTo({
			url:"./detail?id=" + id
		})
}
App.mpType = 'app'

const app = new Vue({
	store,
    ...App
})
app.$mount()
